import 'package:video_tok/domain/entities/video_post.dart';

abstract class VideoPostRepository {
  //
  // final VideoPostDatasource videosDatasource;

  // VideoPostRepository({required this.videosDatasource});
  // //
  //
  Future<List<VideoPost>> getFavoriteVideosByUser(String userID);
  Future<List<VideoPost>> getTrendingVideosByPage(int page);
}
