import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:video_tok/config/theme/app_theme.dart';
import 'package:video_tok/infraestructure/datasources/local_video_datasource_impl.dart';
import 'package:video_tok/infraestructure/repositories/video_posts_repository_impl.dart';
import 'package:video_tok/presentation/providers/discover_provider.dart';
import 'package:video_tok/presentation/screens/discover/discover_screen.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final videoPostRepository = VideoPostsRepositoryImpl(
        videoPostDatasource: LocalVideoDatasourceImpl());

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          lazy: false,
          create: (_) =>
              DiscoverProvider(videPostsRepository: videoPostRepository)
                ..loadNextPage(),
        )
      ],
      child: MaterialApp(
          title: 'Video Tok',
          debugShowCheckedModeBanner: false,
          theme: AppTheme().getTheme(),
          home: const DiscoverScreen()),
    );
  }
}
