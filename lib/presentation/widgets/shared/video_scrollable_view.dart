import 'package:flutter/material.dart';
import 'package:video_tok/domain/entities/video_post.dart';
import 'package:video_tok/presentation/widgets/shared/video_buttoms.dart';
import 'package:video_tok/presentation/widgets/video/full_screen_player.dart';
import 'package:video_tok/presentation/widgets/video/video_background.dart';

class VideoScrollableView extends StatelessWidget {
  final List<VideoPost> videos;

  const VideoScrollableView({super.key, required this.videos});

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      itemBuilder: (context, index) {
        final VideoPost videoPost = videos[index];

        return Stack(
          children: [
            // video player + gradiente
            SizedBox.expand(
              child: FullScreenPlayer(
                  caption: videoPost.caption, videoUrl: videoPost.videoUrl),
            ),

            // Gradiente
            VideoBackground(stops: const [0.8, 1.0]),
            // Botones
            Positioned(
              bottom: 40,
              right: 20,
              child: VideoButtoms(video: videoPost),
            ),
          ],
        );
      },
      scrollDirection: Axis.vertical,
      physics: const BouncingScrollPhysics(),
      itemCount: videos.length,
    );
  }
}
